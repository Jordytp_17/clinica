/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author LENOVO
 */
public class Patient implements Serializable {

    private String nombres;
    private String apellidos;
    private double descuento;
    private String OrderedDate;
    private String deliverDate;
    private ArrayList examenes;
    

    public ArrayList getExamenes() {
        return examenes;
    }

    public void setExamenes(ArrayList examenes) {
        this.examenes = examenes;
    }

    public String getOrderedDate() {
        return OrderedDate;
    }

    public void setOrderedDate(String OrderedDate) {
        this.OrderedDate = OrderedDate;
    }

    public String getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(String deliverDate) {
        this.deliverDate = deliverDate;
    }
    public double cordobas;
    private double dolares;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getCordobas() {
        return cordobas;
    }

    public void setCordobas(double cordobas) {
        this.cordobas = cordobas;
    }

    public double getDolares() {
        return dolares;
    }

    public void setDolares(double dolares) {
        this.dolares = dolares;
    }

    @Override
    public String toString() {
        return "Patient{" + "nombres=" + nombres + ", apellidos=" + apellidos + ", descuento=" + descuento + ", OrderedDate=" + OrderedDate + ", deliverDate=" + deliverDate + ", examenes=" + examenes + ", cordobas=" + cordobas + ", dolares=" + dolares + '}';
    }

}
