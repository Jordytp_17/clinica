/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Exams implements Serializable{
    private String AddExams;
    private String PriceExams;

    public String getAddExams() {
        return AddExams;
    }

    public void setAddExams(String AddExams) {
        this.AddExams = AddExams;
    }

    public String getPriceExams() {
        return PriceExams;
    }

    public void setPriceExams(String PriceExams) {
        this.PriceExams = PriceExams;
    }

    @Override
    public String toString() {
        return "Exams{" + "AddExams=" + AddExams + ", PriceExams=" + PriceExams + '}';
    }
    
    
    
}
