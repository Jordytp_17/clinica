/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Vector;

/**
 *
 * @author user
 */
public class UsersModel {

    private String nick;
    private String password;
    private String rango;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRango() {
        return rango;
    }

    public static int VerificarUsuarioNuevo(String usuario) {
        Vector lista = mostrar();
        UsersModel obj;
        for (int i = 0; i < lista.size(); i++) {
            obj = (UsersModel) lista.elementAt(i);
            if (obj.getNick().equalsIgnoreCase(usuario)) {
                return i;
            }
        }
        return -1;
    }

    public static int VerificarIngreso(String usuario, String Contraseña) {
        Vector lista = mostrar();
        UsersModel obj;
        for (int i = 0; i < lista.size(); i++) {
            obj = (UsersModel) lista.elementAt(i);
            if (obj.getNick().equalsIgnoreCase(usuario) || obj.getPassword().equalsIgnoreCase(Contraseña)) {
                return i;
            }
        }
         return -1;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

    public static Vector mostrar() {
        return UserList.mostrar();
    }
}
