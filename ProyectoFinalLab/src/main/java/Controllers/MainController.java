/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Patient;
import Util.FileUtil;
import Views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author LENOVO
 */
public class MainController implements ActionListener, FocusListener {

    MainFrame mainFrame;

    JComboBox nameExamComboBox;
//    JComboBox priceNameExamComboBox;
    JList<String> jlistexamenes;
    JFileChooser d;
    Patient patient;

    private String[] exams;
    DefaultComboBoxModel ExamenesComboBoxModel;

    // Modelo de la lista
    private final DefaultListModel<String> listModel = new DefaultListModel<>();

    public JList getJlistexamenes() {
        return jlistexamenes;
    }

    public void setJlistexamenes(JList jlistexamenes) {
        this.jlistexamenes = jlistexamenes;
        // establecemos modelo a la lista
        jlistexamenes.setModel(listModel);
    }

    public MainController(MainFrame f) {
        super();
        mainFrame = f;
        d = new JFileChooser();
        // instanciamos la lista
        jlistexamenes = new JList();
        loadComboboxExams();
    }

    private void loadComboboxExams() {
        // llenamos el arreglo de examenes
        List<String> lines = FileUtil.readFile(Paths.get("files/Exams.txt"));
        exams = new String[lines.size()];
        String temp[] = lines.toArray(exams);
        exams = temp;
        // le pasamos los examenes al combobox
        ExamenesComboBoxModel = new DefaultComboBoxModel<>(exams);
    }

    public void setPerson(Patient b) {
        patient = b;
    }

    public void setPerson(String filePath) {
        File f = new File(filePath);
        readPatient(f);
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }

    public void setMainFrame(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    public JComboBox getNameExamComboBox() {
        return nameExamComboBox;
    }

    public void setNameExamComboBox(JComboBox nameExamComboBox) {
        this.nameExamComboBox = nameExamComboBox;
    }

//    public JComboBox getPriceNameExamComboBox() {
//        return priceNameExamComboBox;
//    }
//
//    public void setPriceNameExamComboBox(JComboBox priceNameExamComboBox) {
//        this.priceNameExamComboBox = priceNameExamComboBox;
//    }
    public JFileChooser getD() {
        return d;
    }

    public void setD(JFileChooser d) {
        this.d = d;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String[] getExams() {
        return exams;
    }

    public void setExams(String[] exams) {
        this.exams = exams;
    }

    public void setExamComboBox(JComboBox jcombo) {
        nameExamComboBox = jcombo;
//        JComboBox nameExamComboBox = new JComboBox(exams);

        nameExamComboBox.setModel(ExamenesComboBoxModel);

    }

    public MainController(MaskFormatter mask) throws ParseException {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "addExam":
                String item = nameExamComboBox.getSelectedItem().toString();
                if (!listModel.contains(item)) {
                    listModel.addElement(item);
                } else {
                    JOptionPane.showMessageDialog(mainFrame, item + " ya esta agregado a la lista!");
                }
                break;
            case "Eliminar Examen":
                int indexToRemove = jlistexamenes.getSelectedIndex();
                if (indexToRemove >= 0) {
                    listModel.remove(indexToRemove);
                }
                break;
            case "save":
                d.showSaveDialog(mainFrame);
                patient = mainFrame.getPersonData();
                writePatient(d.getSelectedFile());
                break;
            case "open":
                d.showOpenDialog(mainFrame);
                patient = readPatient(d.getSelectedFile());
                mainFrame.setPersonData(patient);
                System.out.println("funciona");
                break;
            case "Editar":
                // ----
                ExamController controller = new ExamController();
                controller.showDialog(mainFrame);
                List<String> lines = FileUtil.readFile(Paths.get("files/Exams.txt"));
                exams = new String[lines.size()];
                String temp[] = lines.toArray(exams);
                exams = temp;
                // le pasamos los examenes al combobox
                ExamenesComboBoxModel = new DefaultComboBoxModel<>(exams);
                nameExamComboBox.setModel(ExamenesComboBoxModel);

                
                break;
            case "clear":
                mainFrame.clear();
                break;
            case "close":
                System.exit(0);
                break;

        }
    }

    private void writePatient(File file) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(getPatient());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Patient readPatient(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Patient) ois.readObject();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(mainFrame, e.getMessage(), mainFrame.getTitle(), JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private Patient[] readPatientList(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
        Patient[] mainFrame;
        try (FileInputStream in = new FileInputStream(file); ObjectInputStream s = new ObjectInputStream(in)) {
            mainFrame = (Patient[]) s.readObject();
        }
        return mainFrame;
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {

    }

}
