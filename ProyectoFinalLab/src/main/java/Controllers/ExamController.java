/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Util.FileUtil;
import Views.AddExams;
import Views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Paths;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class ExamController implements ActionListener {

    private AddExams examsDialog;
    private DefaultListModel<String> model;

    public ExamController() {

    }

    private void initController() {
        examsDialog.getBtnAdd().addActionListener(this);
        examsDialog.getBtnDelete().addActionListener(this);
        examsDialog.getBtnSave().addActionListener(this);
        model = new DefaultListModel<>();
        List<String> lines = FileUtil.readFile(Paths.get("files/Exams.txt"));
        String currentExams[] = new String[lines.size()];
        String temp[] = lines.toArray(currentExams);
        currentExams = temp;
        for (String currentExam : currentExams) {
            if (!currentExam.equals("Seleccionar")) {
                model.addElement(currentExam);
            }
        }
        examsDialog.getExamsList().setModel(model);
    }

    public void showDialog(MainFrame frame) {
        examsDialog = new AddExams(frame, true);
        initController();
        examsDialog.setVisible(true);
    }

    /**
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Agregar":
                addNewExam();
                break;
            case "Eliminar":
                deleteExam();
                break;
            case "Guardar":
                saveExams();
                break;
            default:
                break;
        }
    }

    private void addNewExam() {
        String exam = examsDialog.getTxtExamName().getText();
        //double price = Double.parseDouble(examsDialog.getTxtPrice().getText());
        model.addElement(exam);
        examsDialog.getTxtExamName().setText("");
        examsDialog.getTxtPrice().setText("");
        examsDialog.getExamsList().setSelectedValue(exam, true);
    }

    private void deleteExam() {
        model.removeElement(examsDialog.getExamsList().getSelectedValue());
    }

    private void saveExams() {
        
        String currentExams[] = new String[model.getSize() +1];
        currentExams[0] = "Seleccionar";
        for (int i = 1; i < model.getSize()+1; i++) {
            currentExams[i] = String.valueOf(model.toArray()[i-1]);
        }
        FileUtil.writeLines(Paths.get("files/Exams.txt"), currentExams);
        examsDialog.getTxtExamName().setText("");
        examsDialog.getTxtPrice().setText("");
        examsDialog.getExamsList().setSelectedIndex(0);
        JOptionPane.showMessageDialog(examsDialog, "Examenes guardados!", 
                "Atención", JOptionPane.INFORMATION_MESSAGE);
    }
}
