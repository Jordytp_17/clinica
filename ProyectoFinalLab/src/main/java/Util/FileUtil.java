/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wong
 */
public class FileUtil {

    /**
     * Constructor privado
     */
    private FileUtil() {

    }

    /**
     * Leemos todo el contenido del archivo y lo devolvemos en una lista
     *
     * @param path
     * @return
     */
    public static List<String> readFile(Path path) {
        try {
            return Files.readAllLines(path);
        } catch (IOException ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Collections.emptyList();
    }
    
    /**
     * 
     * @param path
     * @param lines 
     */
    public static void writeLines(Path path, String lines[]) {
        try(BufferedWriter writer = Files.newBufferedWriter(path)) {
            writer.write("");
            for (String line : lines) {
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
